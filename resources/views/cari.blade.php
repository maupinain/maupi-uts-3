@extends('index')

@section('isi')
<div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h4 class="card-title"> DATA USER</h4>
          </div>

          {{-- DISINI --}}
          <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    @foreach ($cari as $item)
                        <tr>
                            <th>ID : </th>
                            <td>{{ $item->id }}</td>
                        </tr>
                        <tr>
                            <th>user : </th>
                            <td>{{ $item->user }}</td>
                        </tr>
                        <tr>
                            <th>email : </th>
                            <td>{{ $item->user }}@database.com</td>
                        </tr>
                        <tr>
                            <th>pass : </th>
                            <td>{{ $item->pass }}</td>
                        </tr>
                        <tr>
                            <th>website : </th>
                            <td>www.{{ $item->website }}.com</td>
                        </tr>
                        <tr>
                            <th>Option</th>
                            <td>
                                <a href="/data">Kembali</a>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
