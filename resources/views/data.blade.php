@extends('index')

@section('isi')
<div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h4 class="card-title"> DATA HOSTING</h4>
          </div>

          {{-- DISINI --}}
          <div class="card-body">
            <div class="table-responsive">
              <table border="5px" class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                      <tr>
                          <th>ID</th>
                          <th>User</th>
                          <th>Email</th>
                          <th>Pass</th>
                          <th>Website</th>
                          <th>Option</th>
                      </tr>
                  </thead>
                  <tbody>
                      @foreach ($allitem as $item)
                          <tr align="center">
                              <td>{{ $item->id }}</td>
                              <td>{{ $item->user }}</td>
                              <td>{{ $item->user }}@database.com</td>
                              <td>{{ $item->pass }}</td>
                              <td>www.{{ $item->website }}.com</td>
                              <td>
                                  <a href="{{ URL::to('data', $item->id) }}">Cek</a>
                                  <a href="{{ URL::to('edit', $item->id) }}">Edit</a>
                                  <a href="{{ URL::to('delete', $item->id) }}">Hapus</a>
                              </td>
                          </tr>
                      @endforeach
                  </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
