@extends('index')

@section('isi')
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
            <h4 class="card-title"> DATA USER</h4>
        </div>
        
        {{-- DISINI --}}
          <div class="card-body">
              <div class="table-responsive">
                  <form action="simpan" method="post">
                      {{ csrf_field() }}
                      <table border="5px" class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                          <tr>
                              <th>User :</th>
                              <td>
                                  <input type="text" name="user" id="user">
                              </td>
                          </tr>
                          <tr>
                              <th>Password : </th>
                              <td>
                                  <input type="text" name="pass" id="pass">
                              </td>
                          </tr>
                          <tr>
                              <th>Nama Website : </th>
                              <td>
                                  <input type="text" name="website" id="website">
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <a href="/data">Kembali</a>
                              </td>
                              <td>
                                  <button>BUAT</button>
                              </td>
                          </tr>
                      </table>
                  </form>                  
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection


