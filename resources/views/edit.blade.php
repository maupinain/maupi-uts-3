@extends('index')

@section('isi')
<div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h4 class="card-title"> DATA USER</h4>
          </div>

          {{-- DISINI --}}
          <div class="card-body">
            <div class="table-responsive">
                <form action="{{ url('update', $edit->id) }}" method="post">
                    {{ csrf_field() }}
                    <table border="5px" class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <tr>
                            <th>user :</th>
                            <td>
                                <input type="text" name="user" id="user" value="{{ $edit->user }}">
                            </td>
                        </tr>
                        <tr>
                            <th>pass :</th>
                            <td>
                                <input type="text" name="pass" id="pass" value="{{ $edit->pass }}">
                            </td>
                        </tr>
                        <tr>
                            <th>website :</th>
                            <td>
                                <input type="text" name="website" id="website" value="{{ $edit->website }}">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="/data">Kembali</a>
                            </td>
                            <td>
                                <button>EDIT</button>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
