<div class="sidebar" data-color="white" data-active-color="danger">
    <div class="logo">
      <a href="https://www.creative-tim.com" class="simple-text logo-mini">
        <div class="logo-image-small">
          <img src="../assets/img/logo-small.png">
        </div>
        <!-- <p>CT</p> -->
      </a>
      <a href="https://www.creative-tim.com" class="simple-text logo-normal">
        Creative Tim
        <!-- <div class="logo-image-big">
          <img src="../assets/img/logo-big.png">
        </div> -->
      </a>
    </div>
    <div class="sidebar-wrapper">
      <ul class="nav">
        <li class="active ">
          <a href="/">
            <i class="nc-icon nc-bank"></i>
            <p>Dashboard</p>
          </a>
        </li>
        <li>
          <a href="/data">
            <i class="nc-icon nc-tile-56"></i>
            <p>CRUD DATA</p>
          </a>
        </li>
        <li>
          <a href="/buat">
            <i class="nc-icon nc-tile-56"></i>
            <p>BUAT DATA</p>
          </a>
        </li>
      </ul>
    </div>
  </div>