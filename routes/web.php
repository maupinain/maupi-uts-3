<?php

use App\Http\Controllers\ServerController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('content');
});

Route::get('/tabel', function () {
    return view('tables');
});

Route::get('data', [ServerController::class, 'index']);
Route::get('data/{id}', [ServerController::class, 'show']);
Route::get('buat', [ServerController::class, 'create']);
Route::post('simpan', [ServerController::class, 'store']);
Route::get('edit/{id}', [ServerController::class, 'edit']);
Route::post('update/{id}', [ServerController::class, 'update']);
Route::get('delete/{id}', [ServerController::class, 'destroy']);
